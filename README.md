# Myserver

## Introduction

Ce projet de déployer un serverLinux

## Instructions
clonez le projet dans votre dossier personnel:

̀`̀̀̀ `
git clone https://gitlab.com/petit_bou/myserver.git
```

Entrez maintenant dans le dossier téléchargé:
  cd myserver

vous pouvez maintenant:
- installer un server web
-installer un server ssh

### Instaler un server web Nginx

### Quelques éléments sur le réseau
Pour connaitre son adresse ip
̀```
ip
```
On cherche la ligne qui contien inet (`ip a |grep inet`)

Ceci est l'adresse de notre ordinateur sur le réseau internee.

A ne pas confondre avec adresse ip publique,
qui est l'adresse de votre BOX, accessible depuis Iternet.

Pour obtenir cette dernière, tapez "vhat is my ip?" dans la barre de recherche Google


##### Connaitre ses noms d'hôte

On utilise le fichier `/etc/hosts`,
qui fonctionne comme un server DNS: il s'agit d'un tableau de correspondance
entre des adresses IP et des nom d'hôte.

On va ainsi pouvoir comminuquer avec l'ordinateur un utilisant au choix
l'dresse IP ou le nom de l'hôte.

Ex:
```
127.0.0.1      localhost
127.0.1.1      pavilion
```


##### Installer ngnix
```
sudo apt install ngninx
```
Pour vérifier que le server fonctionne , on utilise
```
sudo systemctl status nginx
```
si le status de nginx est "Inactive", on le demarre avec
```
Sudo sudo systemctl status nginx

```
#####"
la page par défaut d'accès à nginx se trouve à /var/www/html/index.ngnix-debian;html`
 modifiez ce fichier pour changer la page d'acceuil.

```
sudo nano /var/www/html/index.nginx-debian.html
̀̀̀̀```
